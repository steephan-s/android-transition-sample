package com.appsinfinity.www.sample;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RelativeLayout rv_main_container;
    Integer[] anglesArray = {45, 135, 225, 315};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv_main_container = (RelativeLayout) findViewById(R.id.rv_container);
        createManualControls(anglesArray);
    }

    private void createManualControls(final Integer[] angles){
        final List<ImageView> list = new ArrayList<ImageView>();
        for (int i=0; i<angles.length; i++) {
            final LinearLayout row = new LinearLayout(this);
            row.setOrientation(LinearLayout.HORIZONTAL);

            FrameLayout.LayoutParams params = new FrameLayout
                    .LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT);
            params.rightMargin = dp2px(80, this);
            row.setLayoutParams(params);

            final ImageView stateImage = new ImageView(this);
            list.add(i, stateImage);
            LinearLayout.LayoutParams stateImageParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            stateImageParams.height = dp2px(35, this);
            stateImageParams.width = dp2px(35, this);
            stateImage.setLayoutParams(stateImageParams);
            stateImage.setImageResource(R.drawable.ic_red);

            ImageView arrowImage = new ImageView(this);
            LinearLayout.LayoutParams arrowImageParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            arrowImageParams.height = dp2px(30, this);
            arrowImageParams.width = dp2px(30, this);
            arrowImageParams.leftMargin = dp2px(10, this);
            arrowImage.setLayoutParams(arrowImageParams);
            arrowImage.setImageResource(R.drawable.ic_arrow);

            row.addView(stateImage);
            row.addView(arrowImage);
            rv_main_container.addView(row);

            final RotateAnimation rotateAnim = new RotateAnimation(0.0f, angles[i],
                    RotateAnimation.RELATIVE_TO_SELF, 1.5f,
                    RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            rotateAnim.setDuration(1000);
            rotateAnim.setFillAfter(true);
            row.setAnimation(rotateAnim);
            rotateAnim.start();

        }

        for(int j=0; j<angles.length; j++){
            ImageView imageView = list.get(j);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(MainActivity.this, "button clicked", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private Integer dp2px(int dp, Context context){
        float d = context.getResources().getDisplayMetrics().density;
        return  (int)(dp * d);

    }
}
